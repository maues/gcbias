#!/usr/bin/python3
import os
import sys
import time
import pandas as pd
import json
import shutil
global path

path = os.path.dirname(os.path.realpath(__file__))
help_message = "GCBias 1.0 \n \
    how to use: \n \
    \t gcbias [args] ... \n \
    args: \n\
        read1 path_read1.fastq - path of fastq file \n \n\
        read2 path_read2.fastq - path of fastq reverse file if  paired-end \n \n\
        project project_name - name of project \n \n\
        reference reference.fasta - reference genome to alingn [optional] [recomended] \n \n\
        gff reference.gff - [optional] gff file to use in quast \n \n\
        email 'user@email.com' - email to notify when task over \n \n\
    "

def write_file(filename, data, mode="w"):
    with open(filename, mode) as out:
        out.write(data)


def fasta_rename(fasta, outfile, name = 'id_'):
    cmd = "awk \'/^>/{print \">" + name + "\" ++i; next}{print}\' < " + fasta + " > " + outfile
    os.system(cmd)
    
    return os.path.abspath(outfile)


def bowtie2(read1, reference, project, read2 = None, N='1', L='22', threads='16'):
    print("bowtie2...")
    """
    Run bowtiw2 and returns sam file
    """
    out = project + 'bowtie/'
    database = out + "database"
    if not os.path.exists(out):
        os.mkdir(out)
    os.system("bowtie2-build " + reference + " " + database + " >  " + out + 'database.log')
    
    cmd = "bowtie2 -p " + threads + " -x " + database 
    if read2:
        cmd += " -1 " + read1 + " -2 " + read2

    else:
        cmd += " -U " + read1
        
    cmd += " -N " + N + " -L " + L + " -S " + out + "output.sam > " + out + 'execution.log'

    write_file(out + 'comandline.txt', cmd)

    os.system(cmd)

    return out + 'output.sam'


def samtools(samfile, project):
    """
    receive  bowtie sam file and return sorted bam file
    """
    out = project + 'samtools/'
    if not os.path.exists(out):
        os.mkdir(out)
    outbam = out + "output.bam"
    sortedbam = out + "output_sorted.bam"
    os.system(path + "/samtools view -Sb " + samfile + "  > " + outbam)
    os.system(path + "/samtools sort " + outbam + " -o " + sortedbam)
    os.system(path + "/samtools index " + sortedbam)

    return sortedbam


def get_argx(argx):
    x = {'read1' : None , 'read2' : None, 'reference' : None, 'project' : None, 'email' : None, 'gff' : None, 'threads' : '16', 'outcontig' : None, 'identity' : '100'}
    
    for a in argx:
        if a in x:
            x[a] = argx[argx.index(a) + 1]

    return x


def picardio(bamfile, reference, project):
    out = project + "picardio/"
    if not os.path.exists(out):
        os.mkdir(out)
    
    cmd = "java -jar " + path +  "/picardio/CollectGcBiasMetrics.jar I=" \
        + bamfile + " O=" + out + "output_picard CHART=" \
        + out + "Output.pdf S=" + out + "output_summary.txt R=" \
        + reference + " AS=true > " + out + "execution.log" 
    
    write_file(out + 'comdanline.txt', cmd)

    os.system(cmd)


    return out + 'output_picard'


def organism_info(reference, project):
    x = open(reference)
    seq = ''
    cab = x.readline()[1:].strip()
    for line in x.readlines():
        seq += line.strip()

    info = cab + "\t" + str(len(seq))
    with open(project + 'organism.txt', 'w') as org:
        org.write(info)

    return project + 'organism.txt'


def bedtools(orgtxt, reference, project):
    out = project + 'bedtools/'
    bedfile = out + "window_100.bed"
    gcfile = out + "gc_100.txt"
    if not os.path.exists(out):
        os.mkdir(out)

    os.system("bedtools makewindows -g " + orgtxt + " -w 100 >  " + bedfile)
    os.system("bedtools nuc -fi " + reference +
              " -bed " + bedfile + " > " + gcfile)

    return gcfile


def calc_quartile(gcfile, sep='\t'):
    gclist = []
    quartile = {}
    pc = open(gcfile)

    while True:
        line = pc.readline()
        if line[0] == 'G':
            break

    for line in pc.readlines():
        tab = line.strip().split(sep)
        if len(line) > 1:
            gclist.append(float(tab[4].replace(',', '.')))

    df = pd.DataFrame(gclist)
    quartile['25'] = df.quantile(.25).values[0]
    quartile['40'] = df.quantile(.4).values[0]
    quartile['50'] = df.quantile(.5).values[0]
    quartile['60'] = df.quantile(.6).values[0]
    quartile['75'] = df.quantile(.75).values[0]
    
    IQR = quartile['75'] - quartile['25']
    quartile['GCBias'] = quartile['50'] - IQR
    
    return quartile


def scatter_plot(output_picard, gcbias, project):
    plot = {'high_gc': {'x_axis': [], 'y_axis': [], 'error': []},
            'low_gc': {'x_axis': [], 'y_axis': [], 'error': []}}
    pc = open(output_picard)

    while True:
        line = pc.readline()
        if line[0] == 'G':
            break

    for line in pc.readlines():
        tab = line.strip().split("\t")
        if len(line) > 1:
            if float(tab[4].replace(',','.')) > 1:
                plot['high_gc']['x_axis'].append(int(tab[0].replace(',','.')))
                plot['high_gc']['y_axis'].append(float(tab[4].replace(',','.')))
                plot['high_gc']['error'].append(float(tab[5].replace(',','.')))
            else:
                plot['low_gc']['x_axis'].append(int(tab[0].replace(',','.')))
                plot['low_gc']['y_axis'].append(float(tab[4].replace(',','.')))
                plot['low_gc']['error'].append(float(tab[5].replace(',','.')))

    plot['gcbias'] = gcbias['GCBias']
    plot['cut'] = gcbias['50']
    json_data = json.dumps(plot)
    with open(project + 'plot.json', 'w') as out:
        out.write(json_data)

    return project + 'plot.json'


def read_picard(output_picard, cov=1):
    """
        lê a saida do picardio e pega a porcentagem
        de gc que está com cobertura a baixo do parametro cov
    """
    x = []
    pc = open(output_picard)
    cut = {}
    while True:
        line = pc.readline()
        if line[0] == 'G':
            break

    for line in pc.readlines():
        tab = line.strip().split("\t")
        if len(line) > 1:
            x.append([int(tab[0].replace(',','.')), float(tab[4].replace(',','.'))])

    i = 0

    start = 0
    end = 0
    for e in x:
        if e[1] < cov:
            if start == 0:
                start = e[0]
        else:
            if start != 0:
                end = e[0] - 1
                cut[i] = {'start': int(start), 'end': int(end)}
                i += 1
                start = 0
                end = 0

    if end == 0:
        cut[i] = {'start': int(start), 'end': 100}

    return cut


def spades(read1, project, prefix,  read2=None, trusted_contigs=None, S=None, threads='16', untrusted_contigs = None):
    out = project + prefix + 'spades/'
    if not os.path.exists(out):
        os.mkdir(out)

    cmd =  path + '/spades/bin/spades.py -o ' + out + ' -t ' + threads + ' '
    if read2:
        cmd += '-1 ' + read1 + ' -2 ' + read2 + ' '
        if S:
            cmd += '-s ' + S + ' '
    else:
        cmd += '-s ' + read1

    if trusted_contigs:
        cmd += ' --trusted-contigs ' + trusted_contigs + ' '
        
    if untrusted_contigs:
        cmd += ' --untrusted-contigs ' + untrusted_contigs + ' '

    cmd += ' > ' + out + 'execution.log'

    write_file(out + "comandline.txt", cmd)
    os.system(cmd)

    return out + 'contigs.fasta'


def w_log(project, log, start_time):
    elapsed_time = time.time() - start_time
    final = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    Logs = [
        '0',
        '1\t    Start',
        '2\t    Ref assembly',
        '3\t    bowtie2',
        '4\t    samtools',
        '5\t    bedtools',
        '6\t    picardio',
        '7\t     split',
        '8\t    final assembly'
    ]

    with open(project + '/log.txt', 'a+') as logw:
        logw.write(Logs[log] + '\t' + str(final) + '\n')


def start_project(argx):
    project = path + '/projects/' + argx['project'] + '/'
    argx['project'] = project
    
    if not os.path.exists(project):
        os.mkdir(project)
    
    
    if argx['read1']:
        read1 = project + 'read1.fastq'
        shutil.copyfile(argx['read1'], read1)
        argx['read1'] = read1
    
    if argx['read2']:
        read2 = project + 'read2.fastq'
        shutil.copyfile(argx['read2'], read2)
        argx['read2'] = read2
        
    if argx['reference']:
        reference = project + 'reference.fasta'
        shutil.copyfile(argx['reference'], reference)
        argx['reference'] = reference
    
    if argx['gff']:
        gff = project + 'reference.gff'
        shutil.copyfile(argx['gff'], gff)
        argx['gff'] = gff
    
    return argx
        
def dedupe(contig, out, identity = '100'):
    cmd = path + "/bbmap/dedupe2.sh in=" + contig + \
    " out=" + out + " minidentity=" + identity + " overwrite=true ac=f"

    os.system(cmd)
    return out
    
def prinseq(gc_range, project, read1, prefix='', read2=None):
    out = project + prefix + "prinseq/"
    if not os.path.exists(out):
        os.mkdir(out)

    cmd = path + "/prinseq -fastq " + read1 + " "
    if read2:
        cmd += " -fastq2 " + read2 + " "

    for e in gc_range:
        r = str(str(gc_range[e]['start']) + "-" + str(gc_range[e]['end']))
        print((cmd + " -out_good " + out + "reads_out_" + r + " -range_gc " + r))
        os.system(cmd + " -out_good " + out +
                  "reads_out_" + r + " -range_gc " + r + " >> " + out + "execution.log")

    if read2:
        os.system("cat " + out + "*tons* >  " + out + "reads_out_single")
        os.system("rm " + out + "*tons*")

        os.system("cat " + out + "*single* >  " + out + "out_single.fastq")

        os.system("cat " + out + "*_1.fastq >  " + out + "out_1.fastq")
    
        os.system("cat " + out + "*_2.fastq >  " + out + "out_2.fastq")

        os.system("rm " + out + "*reads*")
    else:
        os.system("cat " + out + "reads* > " + out + "out_1.fastq")
        os.system("rm " + out + "*reads*")
        
    return out


def awk(contig_list):
    for contig in contig_list:
        os.system("awk '/^>/{print " + '">Contig0."' + " ++i; next}{print}' < " +
                  contig + " > " + contig + ".mod | mv " + contig + ".mod " + contig)


def gaa(project, t_contigs, contig1, contig2):
    out1 = project + 'merged/'
    if not os.path.exists(out1):
        os.mkdir(out1)

    os.system("perl " + path +  "/gaa/gaa.pl -t " + t_contigs +
              " -q " + contig1 + " -o " + out1)
    os.system("mv " + out1 + "cont* " + project + "merged.fasta")
    os.system("rm " + out1 + "* ")
    os.system("perl " + path + "/gaa/gaa.pl -t " + project +
              "merged.fasta -q " + contig2 + " -o " + out1)

    os.system("mv " + out1 + "merg* " + project + "final.fasta")

    return project + "final.fasta"


def quast(ref_contig, final_contig, ref_fasta, project, gff=None):
    out = project + 'quast/'
    if not os.path.exists(out):
        os.mkdir(out)
    cmd = "quast.py -R " + ref_fasta + \
        " -l Default,Final -m 100 -o " + out + " " + \
        ref_contig + " " + final_contig + " "

    if gff:
        cmd += " -G " + gff

    cmd += " > " + out + "quast.log"

    os.system(cmd)


def save_files(contig_ref, contig_final, project):
    out = project + "download/"
    if not os.path.exists(out):
        os.mkdir(out)

    os.system("cp -r " + contig_ref + " " + contig_final + " " +
              project + "plot.png " + project + "quast " + project + "picardio/output_picard " + project + 'plot.json ' + project + "download/")
    os.system("cd " + project +" && tar -czvf download.tar.gz  download/" )


def plot(output_picard, project):
    tfile = ''
    out_tsv = project + 'picard.tsv'
    pc = open(output_picard)
    while True:
        line = pc.readline()
        if line[0] == 'G':
            tfile = line
            break

    for line in pc.readlines():
        tfile += line

    with open(out_tsv, 'w') as otsv:
        otsv.write(tfile)

    os.system("Rscript " + path + "/plot.R " + out_tsv + " " + project + "plot.png")


def send_mail(email, project):
    os.system("curl 'http://200.239.92.140/GCBias/PHP/send_mail.php?email=" +
              email + "&project=" + project + "'")

def bioawk(std_contigs, project, t_contigs, u_contigs):
    t_contigs = project + "/" + t_contigs
    u_contigs = project + "/" + u_contigs
    
    cmd = path + "/bioawk -c fastx '(length($seq) > 500){ print \">\"$name\"\\n\"$seq }' " + std_contigs + " > " + t_contigs
    os.system(cmd)
    
    cmd = path + "/bioawk -c fastx '(length($seq) < 500){ print \">\"$name\"\\n\"$seq }' " + std_contigs + " > " + u_contigs
    os.system(cmd)
    
    return t_contigs,u_contigs

def redundans(project, read1, contig, read2 = None, threads = '24'):
        out = project + "redundans/"
        cmd = path + "/redundans/redundans.py -i " + read1 + " "
        if read2:
            cmd += read2 + " " 
            
        cmd += " -f " +  contig + " -o " + out + " --identity 1 --overlap 1 --minLength 100  -t " + threads + " > " + project + "redundans_execution.log"
        os.system(cmd)
        write_file(out + "comandline.txt", cmd)
        return out + 'scaffolds.reduced.fa'
    
def main(argx):
    start_time = time.time()
    argx = start_project(argx)
    std_contigs = False
    
    read1 = argx['read1']
    read2 = argx['read2']
    gff = argx['gff']
    project = argx['project']
    reference = argx['reference']
    threads = argx['threads']
    identity = argx['identity']
    print(identity)
    print(argx)
    w_log(project, 1, start_time)
    
    
    if not reference:
        reference = spades(read1, project, 'ref_', read2, threads=threads)
        std_contigs = reference
        
    w_log(project, 2, start_time)
    samfile = bowtie2(read1, reference, project, read2 = read2, threads=threads)

    sortedbam = samtools(samfile, project)
    
    output_picard = picardio(sortedbam, reference, project)
    quartile = calc_quartile(output_picard)
    w_log(project, 3, start_time)
    gc_cut_1 = read_picard(output_picard, quartile['60'])
    gc_cut_2 = read_picard(output_picard, quartile['50'])
    w_log(project, 4, start_time)

    if not std_contigs:
        std_contigs = spades(read1, project, 'ref_', read2, threads=threads)
        
    t_contigs, u_contigs = bioawk(std_contigs, project, 'trusted_contigs.fasta', 'untrusted_contigs.fasta')
        
    w_log(project, 5, start_time)
    out_cut1 = prinseq(gc_cut_1, project, read1, 'cut1_', read2)
    out_cut2 = prinseq(gc_cut_2, project, read1, 'cut2_', read2)
    w_log(project, 6, start_time)
    if read2:
        contig_cut1 = spades(out_cut1 + 'out_1.fastq', project, 'cut1_', out_cut1 +
                             'out_2.fastq', trusted_contigs=t_contigs, S=out_cut1 + 'out_single.fastq', untrusted_contigs= u_contigs, threads=threads)
        contig_cut2 = spades(out_cut2 + 'out_1.fastq', project, 'cut2_', out_cut2 +
                             'out_2.fastq', trusted_contigs=t_contigs, S=out_cut2 + 'out_single.fastq', untrusted_contigs= u_contigs, threads=threads)
    else:

        contig_cut1 = spades(out_cut1 + 'out_1.fastq', project, 'cut1_',
                             trusted_contigs=t_contigs, untrusted_contigs= u_contigs, threads=threads)
        contig_cut2 = spades(out_cut2 + 'out_1.fastq', project, 'cut2_',
                             trusted_contigs=t_contigs, untrusted_contigs= u_contigs, threads=threads)

    w_log(project, 7, start_time)
    awk([std_contigs, contig_cut1, contig_cut2])

    contig_final = gaa(project, std_contigs, contig_cut1, contig_cut2)

    contig_final = dedupe(contig_final, contig_final + '.dedupe.fasta', identity = identity)
    contig_final = fasta_rename(contig_final, project + 'contig_final.fasta')
    plot(output_picard, project)
    quast(std_contigs, contig_final, reference, project, gff)

    scatter_plot(output_picard, quartile, project)
    save_files(std_contigs, contig_final, project)
    
    if  'outcontig' in argx:
        shutil.copyfile(contig_final, argx['outcontig'])
        
    #send_mail(argx['read2'], project)
    w_log(project, 8, start_time)


if 'read1' in sys.argv:
    argx = get_argx(sys.argv)
    print(argx)
    
    main(argx)
else:
    print(help_message)

"""
rm projects/teste/bowtie projects/teste/picardio/  projects/teste/samtools -r
python main.py read1 projects/teste/input/Reads.1.fastq read2 projects/teste/input/Reads.2.fastq reference projects/teste/input/Reference.fasta project projects/teste/
scp -P23 main.py server.py dener@200.239.92.130:/storage1/gcbias/
"""
